from threading import Thread
import network as nw
import decrypt_msg as dec

class IA_Fruit(Thread):

    def __init__(self,name,ip="127.0.0.1",port=1337):
        Thread.__init__(self)
        self.name=name
        self.ip=ip
        self.port=port

    
    def run(self):
        #Connexion au server
        connexion = nw.Network(self.ip,self.port)

        #Envoie du nom
        connexion.send(self.name+"\n")

        #Recuperation du numero de l'equipe
        self.numero = connexion.receive()


        #Boucle principale
        while 1:
            received = connexion.receive()
            if received == "FIN\n":
                break


            print(dec.getDico(received))

            connexion.send("E-E-E\n")

        #print(self.name+" done !")

        
