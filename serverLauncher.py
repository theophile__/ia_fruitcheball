from threading import Thread
import subprocess

class ServerFruitcheBall(Thread):
    def __init__(self,nbJoueurs=4,wait=0):
        Thread.__init__(self)
        self.nbJoueurs=nbJoueurs
        self.wait=wait
    
    def run(self):
        # Lancement du serv
        serv = subprocess.Popen(["java", "-jar","fruitcheball.jar","-nbJoueurs="+str(self.nbJoueurs),"-attente="+str(self.wait),"-laby=2","-log=2","-taille=1"], stdout=subprocess.PIPE)
        out, err = serv.communicate() # on recupere les log

        
        # RECUPERER LE SCORE DES EQUIPES

        scoreIndex = out.find(b"scores finaux") # On recherche finaux dans les 20 dernier charactére
        # Le b sers a transformer la chaine de caractere en bytes array utiliser par la fonction find
       

        scoreString=out[scoreIndex+16:-3].decode("utf-8").split(' ')  # transformer le byte array en string
                                                            #Puis effectuer un split pour chaque espace
        # Rechercher uniquement la chaine contenant les score


        scoreTab=[]
        for nb in scoreString:
            scoreTab.append(int(nb))
        print("Score :")
        print(scoreTab)



    
