

def decrypt_map(msg_map):

    map_diviser=msg_map.split(',')

    msg_dim=map_diviser[0].split(':')
    dim=[int(msg_dim[0]),int(msg_dim[1])]  # dimension de la map

    map_tab=[]

    for y in range(1,dim[1]+1): # Découpage de la map
        map_tab.append([])
        for x in range(dim[0]):
            if map_diviser[y][x]=='.': # 5 pour du vide
                map_tab[y-1].append(5)
            elif map_diviser[y][x]=='X': # 6 pour un mur
                map_tab[y-1].append(6) 
            else:                  # 0 - 4 fruit
                map_tab[y-1].append(int(map_diviser[y][x]))

    return map_tab #,dim
  

def decrypt_equipe(msg_equipe):

    msg_equipe_diviser=msg_equipe.split(',')

    tab_menbres_cord=[]
    tab_menbres_inv=[]
    for i in range(2,5):
        j=msg_equipe_diviser[i].split(':')
        tab_menbres_cord.append([int(j[1]),int(j[2])])
        if j[3] == 'x':
            tab_menbres_inv.append(5) # 5 si le joueur ne possede rien en main
        else:
            tab_menbres_inv.append(int(j[3]))

    tab_bloc_depart=[]
    for i in range(6,9):
        b=msg_equipe_diviser[i].split(':')
        tab_bloc_depart.append([int(b[1]),int(b[2])])
    
    score=msg_equipe_diviser[10]

    tab_fruit_recolter=[]
    for i in range(12,16):
        f=msg_equipe_diviser[i].split(':')
        tab_fruit_recolter.append(int(f[1]))
    
    return [tab_menbres_cord,tab_menbres_inv,tab_bloc_depart,tab_fruit_recolter,score]


def decrypt(msg):

    msg_diviser=msg.split('_')

    numEquipe=int(msg_diviser[0])

    nbEquipe=int(msg_diviser[1])

    map=decrypt_map(msg_diviser[2]) # MAP
    
    tab_equipe=[] #Tableau contenant les information des equipe

    for i in range(3,nbEquipe+3):
        tab_equipe.append(decrypt_equipe(msg_diviser[i]))  
    
    return [numEquipe,nbEquipe,map,tab_equipe]


def getDico(msg):

    tab=decrypt(msg)
    dico={}
    dico["nbEquipe"]=tab[1]
    dico["map"]=tab[2]           #Map : 5 = vide 6 = mur , le reste = fruit 
    dico["equipes"]=[]

    for i in range (dico["nbEquipe"]):
        dicoEquipe={}
        dicoEquipe["position"]=tab[3][i][0]   #Position des menbre de l'equipe [x,y]
        dicoEquipe["inventaire"]=tab[3][i][1] #
        dicoEquipe["depart"]=tab[3][i][2]
        dicoEquipe["fruit"]=tab[3][i][3]
        dicoEquipe["score"]=tab[3][i][4]
        dico["equipes"].append(dicoEquipe)
    
    # Exemple d'utilisation du dico     dico["equipes"][0]["position"] -> [[12,8],[7,9],[4,10]]

    return dico












    

            






